#coding=utf-8
from pylab import *
import matplotlib.pyplot as plt
import os
path1=os.path.abspath('.')

indexs = []
dates = []
def zhexiantu():
    with open(path1+'/keyword/search.txt') as f:
        for result in f.readlines():
            result = result.rstrip('\n')
            result = result.split('\t')
            keyword = result[0]
            index = result[1]
            date = result[2]
            indexs.append(index)
            dates.append(date)

    mpl.rcParams['font.sans-serif'] = ['SimHei']
    names = dates
    x = range(len(dates))
    y = list(map(lambda x:float(x), indexs))
    plt.plot(x,y, marker='o', mec='r', mfc='w',label=keyword)
    plt.legend()  # 让图例生效
    plt.xticks(x, names, rotation=25)
    plt.margins(0)
    plt.subplots_adjust(bottom=0.15)
    plt.xlabel(u"时间") #X轴标签
    plt.ylabel("搜索量") #Y轴标签
    plt.title("搜索量趋势") #标题

    plt.show()


#zhexiantu()