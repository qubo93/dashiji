#coding=utf-8
import json
import urllib.request
import pymysql
import time
import os
path1=os.path.abspath('.')


#定义时间戳
time_value= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
url = 'https://api.shenjian.io/?appid=326457dcbffac78831d27e8edff0664d'
head = {}
head['User-Agent'] ='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36'

with open(path1+'/conf/mysql-connect.conf',encoding='gbk') as f:
    result = f.read()
s = json.loads(result)
host = s['mysql']['host']
port = s['mysql']['port']
user = s['mysql']['user']
password = s['mysql']['password']
database = s['mysql']['database']

def insert_value(rank,keyword,index,time_value):
    db = pymysql.connect(host=host,port=port,user=user,passwd=password,db=database)
    cursor = db.cursor()
    sql ='''insert into search_top(`rank`,`keyword`,`index`,`date`) VALUES('%s','%s','%s','%s')''' %(rank,keyword,index,time_value)
    try:
        cursor.execute(sql)
        db.commit()
        print( rank + " was commited")
    except:
        db.rollback()
        print( rank + " was rollbacked")
    db.close()

def clear_top_50():
    db = pymysql.connect(host=host, port=port, user=user, passwd=password, db=database)
    cursor = db.cursor()
    sql = '''delete from top_50;'''
    try:
        cursor.execute(sql)
        db.commit()
        print("delete was commited")

    except:
        db.rollback()
        print("delete was rollbacked")
        db.close()


def sync_to_top_50(rank, keyword, index, time_value):
    db = pymysql.connect(host=host, port=port, user=user, passwd=password, db=database)
    cursor = db.cursor()
    sql = '''insert into top_50(`rank`,`keyword`,`index`,`date`) VALUES('%s','%s','%s','%s')''' % (
    rank, keyword, index, time_value)

    try:
        cursor.execute(sql)
        db.commit()
        print(rank + " sync top_50 was commited")
    except:
        db.rollback()
        print(rank + " sync top_50 was rollbacked")
    db.close()


def query_data(url,head):

    req = urllib.request.Request(url,headers=head)
    with urllib.request.urlopen(req) as response:
        the_page = response.read()
    s = json.loads(the_page)
    clear_top_50()
    for i in range(0,50):
        rank = s['data'][i]['rank']
        keyword = s['data'][i]['keyword']
        index = s['data'][i]['index']
        insert_value(rank,keyword,index,time_value)
        sync_to_top_50(rank,keyword,index,time_value)

query_data(url,head)
