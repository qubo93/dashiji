#coding=utf-8
import pymysql
import json

with open('conf/mysql-connect.conf',encoding='gbk') as f:
    result = f.read()
s = json.loads(result)
host = s['mysql']['host']
port = s['mysql']['port']
user = s['mysql']['user']
password = s['mysql']['password']
database = s['mysql']['database']

def top():
    db = pymysql.connect(host=host, port=port, user=user, passwd=password, db=database)
    cursor = db.cursor()

    sql = '''select *  from `top_50` '''

    print('排行\t搜索词\t搜索数量')
    try:
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            rank=row[1]
            keyword=row[2]
            index=row[3]
            #date=row[4]
            print(rank,keyword,index)
    except:
        print("Error: unable to fecth data")

    db.close()